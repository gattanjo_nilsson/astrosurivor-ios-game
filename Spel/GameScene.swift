//
//  GameScene.swift
//  Spel
//
//  Created by ITHS on 2016-04-09.
//  Copyright (c) 2016 ITHS. All rights reserved.
//

import SpriteKit

var time = 0;
var powerUps = [SKNode]()

let background1 = SKSpriteNode(imageNamed: "bg")
let background2 = SKSpriteNode(imageNamed: "bg")
var shieldButton:SKSpriteNode!
var rocketButton:SKSpriteNode!
var rippleButton:SKSpriteNode!
var sprite = SKSpriteNode(imageNamed:"Spaceship")

struct Sounds {
    static let explosionSound = SKAction.playSoundFileNamed("explosion", waitForCompletion:false)
    static let rocketSound = SKAction.playSoundFileNamed("rocket-launcher", waitForCompletion:false)
    static let laserSound = SKAction.playSoundFileNamed("laser.wav", waitForCompletion:false)
    static let errorSound = SKAction.playSoundFileNamed("error.wav", waitForCompletion:false)
    static let activatingSound = SKAction.playSoundFileNamed("activated.wav", waitForCompletion:false)
    static let deactivatingSound = SKAction.playSoundFileNamed("deactivating.wav", waitForCompletion:false)
    static let buttonSound = SKAction.playSoundFileNamed("pushbutton", waitForCompletion: false)
    static let energySound = SKAction.playSoundFileNamed("energy", waitForCompletion: false)
    static let pickUpHealthSound = SKAction.playSoundFileNamed("pick-up-health", waitForCompletion: false)
    static let reloadSound = SKAction.playSoundFileNamed("camera-click-nikon", waitForCompletion: false)
    static let BGMusic = SKAudioNode(fileNamed: "Tally Ho!")
}


var touched = false
var shieldOn = false
var playerDead = false

var shieldCount = 1;
var rocketCount = 1;
var rippleCount = 1;

var timeLabel:SKLabelNode!
var shieldLabel:SKLabelNode!
var rocketLabel:SKLabelNode!
var rippleLabel:SKLabelNode!

let rippleCategory: UInt32 = 0x1 << 4
let rocketCategory: UInt32 = 0x1 << 3
let powerUpCategory: UInt32 = 0x1 << 2
let playerCategory: UInt32 = 0x1 << 1
let asteroidCategory: UInt32 = 0x1 << 0

struct Variables {
    static var highscore = [Int]()
    static func convertTime(time: Int) -> String {
        var str = ""
        let minutes = time/60
        let seconds = time % 60
        if(minutes < 10 && seconds < 10) {
            str = String(format:"0\(minutes):0\(seconds)")
        }
        else if (seconds < 10) {
            str = String(format:"\(minutes):0\(seconds)")
        }
        else if (minutes < 10) {
            str = String(format:"0\(minutes):\(seconds)")
        }
        
        return str
    }
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    override func didMoveToView(view: SKView) {
        
        physicsWorld.contactDelegate = self
        
        shieldButton = SKSpriteNode(imageNamed: "shieldButtonGrey")
        rocketButton = SKSpriteNode(imageNamed: "rocketButtonGrey")
        rippleButton = SKSpriteNode(imageNamed: "rippleButtonGrey")
        shieldCount = 1;
        rocketCount = 1;
        rippleCount = 1;
        playerDead = false
        time = 0
        
        let defaults = NSUserDefaults.standardUserDefaults()
        Variables.highscore = defaults.objectForKey("highscore") as? [Int] ?? [Int]()
        
        self.backgroundColor = SKColor.blackColor()
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        size = screenSize.size
        
        Sounds.BGMusic.runAction(SKAction.play())

        background1.anchorPoint = CGPointMake(0.5, 0)
        background1.position = CGPointMake(self.size.width/2, 0);
        background1.zPosition = -15
        background1.size.height = screenSize.height
        background1.size.width = screenSize.width
        self.addChild(background1)
        
        background2.anchorPoint = CGPointMake(0.5, 0)
        background2.position = CGPointMake(self.size.width/2, background1.size.height)
        background2.zPosition = -15
        background2.size.height = screenSize.height
        background2.size.width = screenSize.width
        self.addChild(background2)
        
        shieldButton.setScale(0.5)
        shieldButton.name = "shieldButton"
        shieldButton.anchorPoint = CGPointZero
        shieldButton.position = CGPointMake(10, 10)
        shieldButton.zPosition = 15
        self.addChild(shieldButton)
        
        rocketButton.setScale(0.5)
        rocketButton.name = "rocketButton"
        rocketButton.anchorPoint = CGPointMake(0.5, 0)
        rocketButton.position = CGPointMake(self.size.width/2, 10)
        rocketButton.zPosition = 15
        self.addChild(rocketButton)
        
        rippleButton.setScale(0.5)
        rippleButton.name = "rippleButton"
        rippleButton.anchorPoint = CGPointMake(1, 0)
        rippleButton.position = CGPointMake(self.size.width-10, 10)
        rippleButton.zPosition = 15
        self.addChild(rippleButton)
        
        timeLabel = SKLabelNode(fontNamed: "Arial")
        timeLabel.zPosition = 16
        timeLabel.text = "00:00"
        timeLabel.fontSize = 20
        timeLabel.position = CGPointMake(screenSize.width/2, screenSize.height - 30)
        self.addChild(timeLabel)
        
        let wait = SKAction.waitForDuration(1.0)
        let increase = SKAction.runBlock({
            time += 1
            timeLabel.text = Variables.convertTime(time)
        })
        let sequence = SKAction.sequence([wait,increase])
        runAction(SKAction.repeatActionForever(sequence))
        
        shieldLabel = SKLabelNode(fontNamed: "Arial")
        shieldLabel.zPosition = 16
        shieldLabel.text = "0"
        shieldLabel.fontSize = 20
        shieldLabel.position = CGPointMake(20, 15)
        self.addChild(shieldLabel)
        
        rocketLabel = SKLabelNode(fontNamed: "Arial")
        rocketLabel.zPosition = 16
        rocketLabel.text = "0"
        rocketLabel.fontSize = 20
        rocketLabel.position = CGPointMake(140, 15)
        self.addChild(rocketLabel)
        
        rippleLabel = SKLabelNode(fontNamed: "Arial")
        rippleLabel.zPosition = 16
        rippleLabel.text = "0"
        rippleLabel.fontSize = 20
        rippleLabel.position = CGPointMake(260, 15)
        self.addChild(rippleLabel)

        sprite.anchorPoint = CGPointMake(0.5, 0.5)
        sprite.position = CGPointMake(self.size.width/2, self.size.height/2)
        sprite.zPosition = 14
        sprite.name = "spaceship"
        let shipTexture = SKTexture(imageNamed: "Spaceship.png")
        sprite.physicsBody = SKPhysicsBody(texture: shipTexture, size: sprite.size)
        sprite.physicsBody?.affectedByGravity = false
        sprite.physicsBody?.dynamic = true
        sprite.physicsBody?.collisionBitMask = 0 //collisions
        sprite.physicsBody?.contactTestBitMask = asteroidCategory
        sprite.physicsBody?.categoryBitMask = playerCategory
        self.addChild(sprite)
        
        rocketLabel.text = String(rocketCount)
        shieldLabel.text = String(shieldCount)
        rippleLabel.text = String(rippleCount)
        
        // preload
        SKAction.playSoundFileNamed("explosion", waitForCompletion: true)
        SKAction.playSoundFileNamed("rocket-launcher", waitForCompletion: true)
        SKAction.playSoundFileNamed("laser.wav", waitForCompletion: true)
        //createExplosion(-1000, y: -1000)
        
        createAsteroid()
    }
    
    func createExplosion(x: CGFloat, y: CGFloat) {
        var explosion = SKSpriteNode(imageNamed:"explosion1")
        explosion.position = CGPointMake(x, y)
        self.addChild(explosion)
        let atlas = SKTextureAtlas(named: "explosion")
        var explosionFrames = [SKTexture]()
        
        
        for index in 2...atlas.textureNames.count {
                explosionFrames.append(atlas.textureNamed("explosion\(index)"))
        }
        
        let explode = SKAction.animateWithTextures(explosionFrames, timePerFrame: 0.1)
        let remove = SKAction.removeFromParent()
        explosion.runAction(Sounds.explosionSound)
        explosion.runAction(SKAction.sequence([explode, remove]))
        
    }

    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            let touchedNode = nodeAtPoint(location)
            if(touchedNode.name == "spaceship") {
                touchedNode.position = location
                touched = true
            }
            else if touchedNode.name == "shieldButton" && !shieldOn {
                if shieldCount > 0 {
                    sprite.texture = SKTexture(imageNamed: "SpaceShipShield")
                    sprite.runAction(Sounds.activatingSound)
                    shieldCount -= 1
                    shieldLabel.text = String(shieldCount)
                    shieldOn = true
                    let shieldActiveTime = SKAction.waitForDuration(5)
                    runAction(shieldActiveTime, completion:{
                        sprite.runAction(Sounds.deactivatingSound)
                        shieldOn = false
                        sprite.texture = SKTexture(imageNamed: "Spaceship")
                    })
                    if(shieldCount == 0) {
                        shieldButton.texture = SKTexture(imageNamed: "shieldButtonGreyDisabled")
                    }
                }
                else {
                    sprite.runAction(Sounds.errorSound)
                }
            }
            else if touchedNode.name == "rocketButton" {
                if rocketCount > 0 && playerDead == false {
                    let rocket = SKSpriteNode(imageNamed: "rocket")
                    rocket.position = sprite.position
                    rocket.zPosition = 13
                    let rocketTexture = SKTexture(imageNamed: "rocket.png")
                    rocket.physicsBody = SKPhysicsBody(texture: rocketTexture, size: rocket.size)
                    rocket.setScale(1)
                    rocket.physicsBody?.affectedByGravity = false
                    rocket.physicsBody?.dynamic = true
                    rocket.physicsBody?.collisionBitMask = 0 //collisions
                    rocket.physicsBody?.contactTestBitMask = asteroidCategory
                    rocket.physicsBody?.categoryBitMask = rocketCategory
                    rocket.runAction(Sounds.rocketSound)
                    let move = SKAction.moveTo(CGPoint(x: rocket.position.x, y: background1.size.height), duration: 3.0)
                    let remove = SKAction.removeFromParent()
                    rocket.runAction(SKAction.sequence([move, remove]))
                    self.addChild(rocket)
                    rocketCount -= 1
                    rocketLabel.text = String(rocketCount)
                    if(rocketCount == 0) {
                        rocketButton.texture = SKTexture(imageNamed: "rocketButtonGreyDisabled")
                    }
                }
                else {
                    sprite.runAction(Sounds.errorSound)
                }
            }
            else if touchedNode.name == "rippleButton" {
                if rippleCount > 0 && playerDead == false {
                    rippleCount -= 1
                    rippleLabel.text = String(rippleCount)
                    let Circle = SKShapeNode(circleOfRadius: 1)
                    Circle.physicsBody = SKPhysicsBody(circleOfRadius: self.size.width/2)
                    Circle.position = sprite.position
                    Circle.zPosition = 13
                    Circle.strokeColor = SKColor.redColor()
                    Circle.glowWidth = 0.3
                    Circle.physicsBody?.affectedByGravity = false
                    Circle.physicsBody?.dynamic = true
                    Circle.physicsBody?.collisionBitMask = 0 //collisions
                    Circle.physicsBody?.contactTestBitMask = asteroidCategory
                    Circle.physicsBody?.categoryBitMask = rippleCategory
                    self.addChild(Circle)
                    let duration: NSTimeInterval = 3
                    let fade = SKAction.fadeOutWithDuration(duration)
                    let scale = SKAction.scaleTo(160.0, duration: duration)
                    fade.timingMode = .EaseOut
                    scale.timingMode = .EaseOut
                    Circle.runAction(fade)
                    Circle.runAction(scale, completion:{
                        Circle.physicsBody = SKPhysicsBody(circleOfRadius: self.size.width/2)
                    })
                    sprite.runAction(Sounds.laserSound)
                    if(rippleCount == 0) {
                        rippleButton.texture = SKTexture(imageNamed: "rippleButtonGreyDisabled")
                    }
                }
                else {
                    sprite.runAction(Sounds.errorSound)
                }
            }
            else {
                touched = false;
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            if(touched) {
                sprite.position = touch.locationInNode(self)
            }
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        background1.position = CGPointMake(background1.position.x, background1.position.y - 6)
        background2.position = CGPointMake(background2.position.x, background2.position.y - 6)
        
        if(background1.position.y < -background1.size.height)
        {
            background1.position = CGPointMake(background1.position.x, background2.position.y + background2.size.height )
        }
        
        if(background2.position.y < -background2.size.height)
        {
            background2.position = CGPointMake(background2.position.x, background1.position.y + background1.size.height)
            
        }
    }
    
    func randomBetweenNumbers(firstNum: CGFloat, secondNum: CGFloat) -> CGFloat{
        return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(firstNum - secondNum) + min(firstNum, secondNum)
    }
    //Helper method for spawning a point along the screen borders. This will not work for diagonal lines.
    func randomPointBetween(start:CGPoint, end:CGPoint)->CGPoint{
        
        return CGPoint(x: randomBetweenNumbers(start.x, secondNum: end.x), y: randomBetweenNumbers(start.y, secondNum: end.y))
        
    }
    
    
    func createAsteroid(){
        
        //Randomize spawning time.
        let wait = SKAction .waitForDuration(1.5, withRange: 0.5)
        
        weak var  weakSelf = self
        
        let spawn = SKAction.runBlock({
            
            let random = arc4random() % 8 +  1
            var position = CGPoint()
            var moveTo = CGPoint()
            
            switch random {
                
            //Top
            case 1...5:
                position = weakSelf!.randomPointBetween(CGPoint(x: 0, y: weakSelf!.frame.height), end: CGPoint(x: weakSelf!.frame.width, y: weakSelf!.frame.height))
                
                //Move to opposite side
                moveTo = weakSelf!.randomPointBetween(CGPoint(x: 0, y: 0), end: CGPoint(x:weakSelf!.frame.width, y:0))
                
                break
                
            //Bottom
            case 6:
                position = weakSelf!.randomPointBetween(CGPoint(x: 0, y: 0), end: CGPoint(x: weakSelf!.frame.width, y: 0))
                
                //Move to opposite side
                moveTo = weakSelf!.randomPointBetween(CGPoint(x: 0, y: weakSelf!.frame.height), end: CGPoint(x: weakSelf!.frame.width, y: weakSelf!.frame.height))
                
                break
                
            //Left
            case 7:
                position = weakSelf!.randomPointBetween(CGPoint(x: 0, y: 0), end: CGPoint(x: 0, y: weakSelf!.frame.height))
                
                //Move to opposite side
                moveTo = weakSelf!.randomPointBetween(CGPoint(x: weakSelf!.frame.width, y: 0), end: CGPoint(x: weakSelf!.frame.width, y: weakSelf!.frame.height))
                
                break
                
            //Right
            case 8:
                position = weakSelf!.randomPointBetween(CGPoint(x: weakSelf!.frame.width, y: 0), end: CGPoint(x: weakSelf!.frame.width, y: weakSelf!.frame.height))
                
                //Move to opposite side
                moveTo = weakSelf!.randomPointBetween(CGPoint(x: 0, y: 0), end: CGPoint(x: 0, y: weakSelf!.frame.height))
                break
                
            default:
                break
                
            }
            
            let random2 = arc4random() % 100 + 1
            if(random2 < 50) {
                weakSelf!.spawnAsteroidAtPosition(position, moveTo: moveTo)
            }
            else {
                weakSelf!.spawnPowerUpAtPosition(position, moveTo: moveTo)
            }
            
            
            
        })
        
        let spawning = SKAction.sequence([wait,spawn])
        
        self.runAction(SKAction.repeatActionForever(spawning), withKey:"spawning")
        
        
    }
    
    func spawnPowerUpAtPosition(position:CGPoint, moveTo:CGPoint){
        let random = arc4random() % 3 + 1
        
        switch random {
        case 1:
            let powerUp = SKSpriteNode(imageNamed: "shieldPowerUp")
                powerUp.name = "shield"
                powerUp.position = position
                let powerUpTexture = SKTexture(imageNamed: "shieldPowerUp")
                powerUp.physicsBody = SKPhysicsBody(texture: powerUpTexture, size: powerUp.size)
                powerUp.physicsBody?.affectedByGravity = false
                powerUp.physicsBody?.dynamic = true
                powerUp.physicsBody?.collisionBitMask = 0 //collisions
                powerUp.physicsBody?.contactTestBitMask = playerCategory
                powerUp.physicsBody?.categoryBitMask = powerUpCategory
                let move = SKAction.moveTo(moveTo, duration: 3.0)
                let remove = SKAction.removeFromParent()
                powerUp.runAction(SKAction.sequence([move, remove]))
                self.addChild(powerUp)
                powerUps.append(powerUp)
        case 2:
            let powerUp = SKSpriteNode(imageNamed: "rocketPowerUp")
                powerUp.name = "rocket"
                powerUp.position = position
                let powerUpTexture = SKTexture(imageNamed: "rocketPowerUp")
                powerUp.physicsBody = SKPhysicsBody(texture: powerUpTexture, size: powerUp.size)
                powerUp.physicsBody?.affectedByGravity = false
                powerUp.physicsBody?.dynamic = true
                powerUp.physicsBody?.collisionBitMask = 0 //collisions
                powerUp.physicsBody?.contactTestBitMask = playerCategory
                powerUp.physicsBody?.categoryBitMask = powerUpCategory
                let move = SKAction.moveTo(moveTo, duration: 3.0)
                let remove = SKAction.removeFromParent()
                powerUp.runAction(SKAction.sequence([move, remove]))
                self.addChild(powerUp)
                powerUps.append(powerUp)
        case 3:
            let powerUp = SKSpriteNode(imageNamed: "ripplePowerUp")
                powerUp.name = "ripple"
                powerUp.position = position
                let powerUpTexture = SKTexture(imageNamed: "ripplePowerUp")
                powerUp.physicsBody = SKPhysicsBody(texture: powerUpTexture, size: powerUp.size)
                powerUp.physicsBody?.affectedByGravity = false
                powerUp.physicsBody?.dynamic = true
                powerUp.physicsBody?.collisionBitMask = 0 //collisions
                powerUp.physicsBody?.contactTestBitMask = playerCategory
                powerUp.physicsBody?.categoryBitMask = powerUpCategory
                let move = SKAction.moveTo(moveTo, duration: 3.0)
                let remove = SKAction.removeFromParent()
                powerUp.runAction(SKAction.sequence([move, remove]))
                self.addChild(powerUp)
                powerUps.append(powerUp)
        default:
            break;
        }
        
    }
    
    func spawnAsteroidAtPosition(position:CGPoint, moveTo:CGPoint){
        
        let asteroid = SKSpriteNode(imageNamed: "asteroid")
        asteroid.position = position
        let asteroidTexture = SKTexture(imageNamed: "asteroid.png")
        asteroid.physicsBody = SKPhysicsBody(texture: asteroidTexture, size: asteroid.size)
        let random = drand48() + 0.5
        asteroid.setScale(CGFloat(random))
        asteroid.physicsBody?.affectedByGravity = false
        asteroid.physicsBody?.dynamic = true
        asteroid.physicsBody?.collisionBitMask = 0 //collisions
        asteroid.physicsBody?.contactTestBitMask = playerCategory
        asteroid.physicsBody?.categoryBitMask = asteroidCategory
        let move = SKAction.moveTo(moveTo, duration: 3.0)
        let rotate = SKAction.rotateToAngle(CGFloat(4.0), duration: 3)
        let remove = SKAction.removeFromParent()
        asteroid.runAction(SKAction.sequence([move, remove]))
        asteroid.runAction(rotate)
        self.addChild(asteroid)
        
    }
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        if (contact.bodyA.categoryBitMask == playerCategory) &&
            (contact.bodyB.categoryBitMask == asteroidCategory) {
            if !shieldOn {
                
                playerDead = true
                let node = contact.bodyA.node!
                node.physicsBody?.contactTestBitMask = 0
                node.physicsBody?.dynamic = false
                node.physicsBody?.collisionBitMask = 0 //collisions
                node.physicsBody?.categoryBitMask = 0
                createExplosion(node.position.x, y: node.position.y)
                sprite.runAction(SKAction.removeFromParent())
                if Variables.highscore.isEmpty {
                        Variables.highscore.append(time)
                }
                
                for(var i = 0; i < Variables.highscore.count; i += 1) {
                    if time >= Variables.highscore[i] {
                        Variables.highscore.insert(time, atIndex: i)
                        break
                    }
                }
                
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(Variables.highscore, forKey: "highscore")
                defaults.synchronize()
                
                let wait = SKAction.waitForDuration(3)
                
                let secondScene = ResultScreenScene(size: self.size)
                let transition = SKTransition.fadeWithColor(SKColor.blackColor(), duration: 5)
                secondScene.scaleMode = SKSceneScaleMode.AspectFill
                
                let trans = SKAction.runBlock({
                    self.scene!.view?.presentScene(secondScene, transition: transition)
                })
                Sounds.BGMusic.runAction(SKAction.stop())
                runAction(SKAction.sequence([wait, trans]))
            }
        }
        
        if (contact.bodyA.categoryBitMask == playerCategory) &&
            (contact.bodyB.categoryBitMask == powerUpCategory) {
            let node = contact.bodyB.node
            
            switch node!.name {
            case "shield"?:
                shieldCount += 1
                runAction(Sounds.pickUpHealthSound)
                if(shieldCount > 0) {
                    shieldButton.texture = SKTexture(imageNamed: "shieldButtonGrey")
                }
                
            case "rocket"?:
                rocketCount += 1
                runAction(Sounds.reloadSound)
                if(rocketCount > 0) {
                    rocketButton.texture = SKTexture(imageNamed: "rocketButtonGrey")
                }
                
            case "ripple"?:
                rippleCount += 1
                runAction(Sounds.energySound)
                if(rippleCount > 0) {
                    rippleButton.texture = SKTexture(imageNamed: "rippleButtonGrey")
                }
                
            default:
                break
            }
            
            node!.runAction(SKAction.removeFromParent())
            node!.physicsBody?.contactTestBitMask = 0
            node!.physicsBody?.dynamic = false
            node!.physicsBody?.collisionBitMask = 0 //collisions
            node!.physicsBody?.categoryBitMask = 0
            
            rocketLabel.text = String(rocketCount)
            shieldLabel.text = String(shieldCount)
            rippleLabel.text = String(rippleCount)
        }
        if (contact.bodyA.categoryBitMask == asteroidCategory) &&
            (contact.bodyB.categoryBitMask == rocketCategory) {
            contact.bodyA.node!.runAction(SKAction.removeFromParent())
            contact.bodyB.node!.runAction(SKAction.removeFromParent())
            let node = contact.bodyA.node!
            createExplosion(node.position.x, y: node.position.y)
        }
        if (contact.bodyA.categoryBitMask == asteroidCategory) &&
            (contact.bodyB.categoryBitMask == rippleCategory) {
            let node = contact.bodyA.node!
            createExplosion(node.position.x, y: node.position.y)
            node.runAction(SKAction.removeFromParent())
        }
    }
}
