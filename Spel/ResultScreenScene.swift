//
//  ResultScreenScene.swift
//  Spel
//
//  Created by ITHS on 2016-04-24.
//  Copyright © 2016 ITHS. All rights reserved.
//

import SpriteKit

class ResultScreenScene: SKScene {
    override func didMoveToView(view: SKView) {
        
        let highScore = Variables.highscore
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        let gameOverLabel = SKLabelNode(fontNamed: "Arial")
        gameOverLabel.position = CGPointMake(screenSize.width/2, screenSize.height - 40)
        gameOverLabel.text = "Game Over"
        self.addChild(gameOverLabel)
        
        let highScoreLabel = SKLabelNode(fontNamed: "Arial")
        highScoreLabel.text = "High Score"
        highScoreLabel.fontSize = 18
        highScoreLabel.position = CGPointMake(screenSize.width/2, screenSize.height - 60)
        self.addChild(highScoreLabel)
        var length = 0
        if(highScore.count > 20) {
            length = 20
        }
        else {
            length = highScore.count
        }
        for (var i = 0; i < length; i += 1) {
            let scoreLabel = SKLabelNode(fontNamed: "Arial")
            scoreLabel.fontSize = 18
            scoreLabel.position = CGPointMake(screenSize.width/2, highScoreLabel.position.y - CGFloat(i+1)*20)
            var str : String = ""
            str += String(format:"\n%d. %@\n", i+1, Variables.convertTime(highScore[i]))
            scoreLabel.text = str
            self.addChild(scoreLabel)
        }
        
        
        self.backgroundColor = SKColor.blackColor()
        let button = SKSpriteNode(imageNamed: "play_again.png")
        button.position = CGPointMake(CGRectGetMidX(self.frame), 60)
        button.name = "replayButton"
        button.setScale(0.7)
        self.addChild(button)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches
        let location = touch.first!.locationInNode(self)
        let node = self.nodeAtPoint(location)
        
        if (node.name == "replayButton") {
            runAction(Sounds.buttonSound)
            let gameScene = GameScene(size: self.size)
            let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.scene!.view?.presentScene(gameScene, transition: transition)
        }
    }
}
